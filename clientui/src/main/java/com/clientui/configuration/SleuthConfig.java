package com.clientui.configuration;

import brave.sampler.Sampler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SleuthConfig {

    /**
     * Class permettant dse choisir quelle requete exporter
     * @return Sampler Objet contenant les infos de tracage
     */
    public Sampler defaultSampler(){
        // Demande que toutes les requêtes soient marquées par des ID
        // et soient exportables vers d'autres services comme Zipkin
        return Sampler.ALWAYS_SAMPLE;
    }
}
