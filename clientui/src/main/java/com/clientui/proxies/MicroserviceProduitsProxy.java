package com.clientui.proxies;

import com.clientui.beans.ProductBean;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

/**
 * @FeignClient déclare cette interface comme client Feign et comporte 2 parametres
 * Le premier est le nom du microservice à renseigner via la variable "spring.application.name"
 * qui se trouve dans le ficher resources/bootstrap.properties du microservice hote à appeler
 * Le deuxieme est l'url du microservice hote
 *
 * Feign utilise ici des infos afin de construire des requets HTTP appropriées pour appeler le MicroService-Produits
 *
 */
//@FeignClient(name = "microservice-produits", url = "localhost:9001")
//@FeignClient(name = "microservice-produits") // url pas utile car Ribbon est capable de sélectionner les bons services pour nous
@FeignClient(name = "zuul-server") // appelle Zuul pour filtrer les requetes clients au lieu de communiquezr directement avec les Microservices
@RibbonClient(name = "microservice-produits") // demander à Ribbon d'aller chercher automatiquement
                                              // la liste des URL (et donc instances) de Microservice-produits disponibles.
public interface MicroserviceProduitsProxy {

    //@GetMapping(value = "/produits")
    @GetMapping(value = "/microservice-produits/produits")
    List<ProductBean> listeDesProduits();

    /*
     * Notez ici la notation @PathVariable("id") qui est différente de celle qu'on utlise dans le contrôleur
     **/
    @GetMapping(value = "/microservice-produits/produits/{id}")
    ProductBean recupererUnProduit(@PathVariable("id") int id);

}
