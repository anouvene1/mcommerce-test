package com.clientui.exceptions;

import feign.Response;
import feign.codec.ErrorDecoder;

/**
 *  Décrypter l'exception (générique de feign: error 500) générée par Feign pour retomber sur les bons codes HTTP renvoyés
 */
public class CustomErrorDecoder implements ErrorDecoder { // On hérite de  ErrorDecoder

    // Décodeur par défaut si l'erreur ne rentre pas dans nos critères
    private final ErrorDecoder defaultErrorDecoder = new Default();

    /**
     * Récupérer un code erreur afin de lancer les exceptions correspondantes
     * @param invoqueur Méthode utilisée pour appeler un microservice
     * ex.: invoqueur => MicroserviceProduitsProxy#recupererUnProduit(int) avec la méthode utilisée "recupererUnProduit"
     *                  pour appeler le micro service "MicroserviceProduitsProxy"
     * @param reponse Contient la réponse du micro-service avec son code d'erreur
     *                response.status contient l'errezur du micro-service distant
     * @return
     */
    @Override
    public Exception decode(String invoqueur, Response reponse) {

        if(reponse.status() == 400 ) { // Si code = 400, levee d'exception en appelant "ProductBadRequestException"
            return new ProductBadRequestException("Requête incorrecte");
        }
        else if (reponse.status() == 404 ) {
            return new ProductNotFoundException("Produit non trouvé !!!");
        }

        // Si l'erreur ne rentre pas dans nos critères, on la passe tout simplement au décodeur par défaut
        // qui s'occupera de lancer l'exception par défaut
        return defaultErrorDecoder.decode(invoqueur, reponse);
    }

}
