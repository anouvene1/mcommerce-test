package com.mproduits;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableConfigurationProperties // Les props de configs
@EnableDiscoveryClient // Microservice se déclarant client d'EUREKA
public class MproduitsApplication {

	public static void main(String[] args) {
		SpringApplication.run(MproduitsApplication.class, args);
	}
}


// Astuce pour créer plusieurs instance de ce micoservice avec IntelliJ
// Copier une config déjà présente puis ajouter l'argument "-Dserver.port=port_du_microservice" dans le champ VM options