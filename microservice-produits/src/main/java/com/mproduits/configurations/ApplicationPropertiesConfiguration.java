package com.mproduits.configurations;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

// import static org.springframework.context.annotation.ScopedProxyMode.DEFAULT;

/**
 * Bean permettant de récuperer les proprietes de configuration du microservice-produits
 * et ce grace a la notation @ConfigurationProperties
 */
@Component
@ConfigurationProperties("products-configs")
// @RefreshScope(proxyMode = DEFAULT) // Indiquer à un de nos beans qui accède aux propriétés de se rafraîchir à chaque fois qu'un événement Refresh est lancé.
@RefreshScope
public class ApplicationPropertiesConfiguration {
    private int limitDeProduits;

    private String msgHello;

    public int getLimitDeProduits() {
        return limitDeProduits;
    }
    public void setLimitDeProduits(int limitDeProduits) {
        this.limitDeProduits = limitDeProduits;
    }

    public String getMsgHello() { return msgHello; }
    public void setMsgHello(String messsage) { this.msgHello = messsage; }
}
