INSERT INTO PRODUCT (ID , TITRE ,DESCRIPTION ,IMAGE ,PRIX ) VALUES (0, 'Bougie fonctionnant au feu', 'bougie qui fonctionne comme une ampoule mais sans électricité !', 'https://cdn.pixabay.com/photo/2013/11/12/01/47/candles-209157_960_720.jpg', 22.0);

INSERT INTO PRODUCT (ID , TITRE ,DESCRIPTION ,IMAGE ,PRIX ) VALUES (1, 'Chaise pour s''assoire', 'Chaise rare avec non pas 1 ni 2 mais 3 pieds', 'https://cdn.pixabay.com/photo/2015/06/19/10/00/summer-814679_960_720.jpg', 95.0);

INSERT INTO PRODUCT (ID , TITRE ,DESCRIPTION ,IMAGE ,PRIX ) VALUES (2, 'Cheval pour nains', 'Ce cheval ne portera certainement pas blanche neige, mais sans problème les nains', 'https://cdn.pixabay.com/photo/2019/02/09/08/58/pony-3984685_960_720.jpg', 360.0);

INSERT INTO PRODUCT (ID , TITRE ,DESCRIPTION ,IMAGE ,PRIX ) VALUES (3, 'Coq of steel, le superman des volailles', 'Ne passe pas au four', 'https://cdn.pixabay.com/photo/2018/08/15/11/49/hahn-3607868_960_720.jpg', 620.0);

INSERT INTO PRODUCT (ID , TITRE ,DESCRIPTION ,IMAGE ,PRIX ) VALUES (4, 'Flacon à frotter avec un génie dedans', 'Vous donne droit à l''équivalent de 3/0 voeux', 'https://cdn.pixabay.com/photo/2017/06/09/00/56/books-2385398_960_720.jpg', 1200.0);

INSERT INTO PRODUCT (ID , TITRE ,DESCRIPTION ,IMAGE ,PRIX ) VALUES (5, 'Horloge quantique', 'Donne l''heure, les minutes et même les secondes. Ne fait pas de café', 'https://cdn.pixabay.com/photo/2018/12/08/10/10/time-3863105_960_720.jpg', 180.0);

INSERT INTO PRODUCT (ID , TITRE ,DESCRIPTION ,IMAGE ,PRIX ) VALUES (6, 'Table d''opération pour Hamsters', 'Pour réaliser vos opérations chirugicales sur votre Hamster!', 'https://cdn.pixabay.com/photo/2015/07/19/10/00/still-life-851328__340.jpg', 210.0);

INSERT INTO PRODUCT (ID , TITRE ,DESCRIPTION ,IMAGE ,PRIX ) VALUES (7 , 'Vase ayant appartenu a Zeus', 'Risque de choc électrique', 'https://cdn.pixabay.com/photo/2018/04/05/14/09/sunflower-3292932_960_720.jpg', 730.0);