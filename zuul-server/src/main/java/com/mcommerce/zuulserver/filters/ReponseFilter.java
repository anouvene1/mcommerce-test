package com.mcommerce.zuulserver.filters;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;

/**
 * Filtre qui s'exécute à la réponse de la requête
 * et récupère toutes les réponses et change le code en 400
 */
@Component
public class ReponseFilter extends ZuulFilter {

    // Afin de logger les requêtes reçues, on crée une instance du logger de SL4J
    Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public String filterType() {
        return "post";
    }

    @Override
    public int filterOrder() {
        return 1;
    }

    /**
     * N'oubliez pas de désactiver ce filtre (shouldFilter à false)
     * pour qu'il ne vous bloque pas pour le reste du cours.
     * @return Boolean
     */
    @Override
    public boolean shouldFilter() {
        return false;
    }

    @Override
    public Object run() throws ZuulException {

        HttpServletResponse response = RequestContext.getCurrentContext().getResponse();

        response.setStatus(400);

        log.info(" CODE HTTP {} ", response.getStatus());


        return null;
    }
}
