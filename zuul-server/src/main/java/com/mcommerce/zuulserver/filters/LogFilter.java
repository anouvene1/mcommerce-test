package com.mcommerce.zuulserver.filters;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;

import org.springframework.stereotype.Component;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Component
public class LogFilter extends ZuulFilter {

    // Afin de logger les requêtes reçues, on crée une instance du logger de SL4J
    Logger log = LoggerFactory.getLogger(this.getClass());

    /**
     * Méthode permettant de déterminer le type de filtre à appliquer, elle propose 4 possibilités
     * pre : permet d'exécuter du code avant la redirection de la requête vers sa destination finale.
     * post : permet d'exécuter du code après que la requête a été redirigée.
     * route : permet d'agir sur la façon de rediriger les requêtes.
     * error : permet d'agir en cas d'erreur lors de la redirection de la requête.
     * @return Une chaine
     */
    @Override
    public String filterType() {
        return "pre";
    }

    /**
     * Dans notre API Gateway ZUUL, nous avons forcément des dizaines de filtres.
     * Cette méthode détermine l'ordre d'exécution de ce filtre.
     * @return Entier
     */
    @Override
    public int filterOrder() {
        return 1;
    }

    /**
     * Méthode permettant d'écrire les conditions qui doivent être remplies pour que le filtre s'exécute.
     * @return Boolean
     */
    @Override
    public boolean shouldFilter() {
        return true;
    }

    /**
     * C'est ici que va la logique de votre filtre
     * @return
     * @throws ZuulException
     */
    @Override
    public Object run() throws ZuulException {
        // On récupère la requête grâce à  RequestContext  qui est utilisé par les filtres dans ZUUL
        // afin de manipuler les requêtes en attendant leur redirection
        HttpServletRequest req = RequestContext.getCurrentContext().getRequest();

        log.info("**** Requête interceptée ! L'URL est : {} " , req.getRequestURL());

        return null;
    }
}
